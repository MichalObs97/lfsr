-----------------------------------------------------------------------
-- Brno University of Technology, Department of Radio Electronics
-----------------------------------------------------------------------
-- Author: Andrej Barc (195267) & Michal Obsitnik (195401)
-- Date: 2019-04-10 14:08
-- Design: top
-- Description: Linear Feedback Shift Register
-----------------------------------------------------------------------
-- TODO: Make an aplication showcasing how does Linear-feedback shift 
--       register works. Make a reset function and make aplication for
--       variable amount of bits
-- NOTE: Longest cycle will occur if the position of XORs is set to  
--       value := x"B4"; Speed of LEDs flashing can be changed by using
--       different value in prescaler.  
-----------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL; -- for +/- arithmetic operations

-- define ports
entity top is            
    port (
        CLK: in std_logic;
        BTN0: in std_logic;
        sw_i  : in std_logic_vector(2-1 downto 0);  -- slide switches
        LED_EXP: out std_logic_vector(12-1 downto 0);
        SW_EXP: in std_logic_vector(16-1 downto 8)
    );
end top;

-- define signals
    architecture Behavioral of top is        
    signal start_state_8: std_logic_vector(8-1 downto 0):= x"47";                 
    signal xor_position_8: std_logic_vector(8-1 downto 0);  --:= x"B4";
    signal start_state_4: std_logic_vector(4-1 downto 0):= "0111";                 
    signal xor_position_4: std_logic_vector(4-1 downto 0);  --:= x"B4";      
    signal clk_500: std_logic := '0';
    signal tmp_500: std_logic_vector(16-1 downto 0):= x"0000";

-- set prescaler of CLK 
begin            
    process (CLK)
    begin
        if rising_edge(CLK) then
            tmp_500 <= tmp_500 + 1;
            if tmp_500 = x"9c4" then
                tmp_500 <= x"0000";
                clk_500 <= not clk_500;
            end if;
        end if;
    end process;

-- Sets XORs to a specefic positions
    process (xor_position_8, xor_position_4)                  
    begin
        if SW_EXP(8) = '0' then
            xor_position_8(0) <= '1';
            else
            xor_position_8(0) <= '0';
        end if;
        
        if SW_EXP(9) = '0' then
            xor_position_8(1) <= '1';
            else
            xor_position_8(1) <= '0';
        end if;
        
        if SW_EXP(10) = '0' then
            xor_position_8(2) <= '1';
            else
            xor_position_8(2) <= '0';
        end if;
        
        if SW_EXP(11) = '0' then
            xor_position_8(3) <= '1';
            else
            xor_position_8(3) <= '0';
        end if;
        
        if SW_EXP(12) = '0' then
            xor_position_8(4) <= '1';
            xor_position_4(0) <= '1';
            else
            xor_position_8(4) <= '0';
            xor_position_4(0) <= '0';
        end if;
        
        if SW_EXP(13) = '0' then
            xor_position_8(5) <= '1';
            xor_position_4(1) <= '1';
            else
            xor_position_8(5) <= '0';
            xor_position_4(1) <= '0';
        end if;
        
        if SW_EXP(14) = '0' then
            xor_position_8(6) <= '1';
            xor_position_4(2) <= '1';
            else
            xor_position_8(6) <= '0';
            xor_position_4(2) <= '0';
        end if;
        
        if SW_EXP(15) = '0' then
            xor_position_8(7) <= '1';
            xor_position_4(3) <= '1';
            else
            xor_position_8(7) <= '0';
            xor_position_4(3) <= '0';
        end if;
    end process;

-- Reset & main LFSR algorithm
  process(clk_500)
  begin
    if sw_i(1) = '0' then  
    -- 8 bit lfsr
        start_state_4<= "0000";                          --turns off 4-bit LEDs
        
       -- Reset and load start polynom => start_state
        if rising_edge(clk_500) then                    
            if BTN0 = '0' then
                start_state_8 <= x"47";
            else
                -- Main LFSR algorithm
                if (start_state_8(0) = '1') then                 
                    start_state_8 <= ('1' & start_state_8(7 downto 1)) xor xor_position_8;
                else
                    start_state_8 <= '0' & start_state_8(7 downto 1);
                end if;
            end if;
        end if;
      
      else  
      -- 4 bit lfsr
      start_state_8<= "00000000";                        --turns off 8-bit LEDs
      
      -- Reset and load start polynom => start_state
        if rising_edge(clk_500) then                    
            if BTN0 = '0' then
                start_state_4 <= "0111";
            else
                -- Main LFSR algorithm
                if (start_state_4(0) = '1') then                 
                    start_state_4 <= ('1' & start_state_4(3 downto 1)) xor xor_position_4;
                else
                    start_state_4 <= '0' & start_state_4(3 downto 1);
                end if;
            end if;
        end if;
    end if;        
  end process;

-- Start LEDs
    LED_EXP(7 downto 0)<= start_state_8;                    
    LED_EXP(11 downto 8)<= start_state_4;
end Behavioral;
